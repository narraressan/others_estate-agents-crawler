
import csv, json, requests

class allHomesComAu:


	file = None


	def __init__(self, csvFile):
		self.file = csvFile


	def getKeywords(self):

		keywords = []

		with open(self.file) as raw:
			rows = csv.reader(raw, delimiter=',')

			rowsCount = 0
			for row in rows:
				if rowsCount > 0: # skip line 0 -> contains heading
					# use only SUBURB
					tmp = row[1].lower().strip()
					if (tmp != '') and (tmp not in keywords):
						keywords.append(tmp)

				rowsCount = rowsCount + 1

			print('Rows: ' + str(rowsCount))
		print('Keywords: ' + str(len(keywords)))

		return keywords


	def searchKeyword(self, keyword):
		collection = []
		req = requests.get('https://www.allhomes.com.au/wsvc/agency/search/autocomplete?query=' + keyword.replace(' ', '%20'))
		res = json.loads(req.text)

		print(req.url + ' - ' + str(len(res['Agency'])))

		for agent in res['Agency']:
			agentData = {
				'company': None if 'agentName' not in agent else agent['agentName'],
				'address': None if 'address' not in agent else agent['address'],
				'suburb': None if 'suburb' not in agent else agent['suburb'],
				'state': None if 'state' not in agent else agent['state'],
				'postcode': None if 'postcode' not in agent else agent['postcode'],
				'phone': None if 'phone' not in agent else agent['phone'],
				'website': None if 'agencyUrl' not in agent else agent['agencyUrl'],
			}
			collection.append(agentData)

		return collection



if __name__ == '__main__':

	# source: https://www.allhomes.com.au/agents/
	# target:
	# 	Company,
	# 	address (street, suburb, state, postcode),
	# 	phone,
	# 	website,
	# 	email -> if available
	crawler = allHomesComAu('./resources/Aust_suburb_postcode.csv')

	collections = []
	keywords = crawler.getKeywords()
	for word in keywords[0:5]:
		collections.extend(crawler.searchKeyword(word))

	writer = csv.writer(open("./output/allHomesComAu.csv", 'w', newline=''))
	writer.writerow(['company'.upper(), 'address'.upper(), 'suburb'.upper(), 'state'.upper(), 'postcode'.upper(), 'phone'.upper(), 'website'.upper()])

	for row in collections:
		print(row)
		try:
			writer.writerow([
				row['company'],
				row['address'],
				row['suburb'],
				row['state'],
				row['postcode'],
				row['phone'],
				row['website']
			])
		except Exception as err:
			print("Err: " + str(err))
